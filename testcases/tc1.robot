*** Settings ***
Library    SeleniumLibrary


*** Variables ***
${browser}     Chrome
${url}     https://qaas.qutrix.io/


*** Test Cases ***
LoginTest
    open browser       ${url}    ${browser}
    sleep    5
    loginapplication
    close browser

*** Keywords ***
loginapplication
    click link    xpath://*[@id="navbar"]/ul/li[5]/a
    click link    link:Ping Us
    sleep    5
    input text    xpath://*[@id="user_query"]/form/div/div/div[2]/div[1]/div/input    jenifajames@gmail.com
    click element    xpath://*[@id="user_query"]/form/div/div/div[2]/div[2]/select
    click element    xpath://*[@id="user_query"]/form/div/div/div[2]/div[2]/select/option[4]
    input text    id:msg_vertical     example
    click element   xpath://*[@id="user_query"]/form/div/div/div[3]/div/div/button[1]/strong


